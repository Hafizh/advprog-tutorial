package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name = "name", required = false)
                                       String name, Model model) {
        if(name == null || name.length() == 0) {
            model.addAttribute("title", "This is my CV");
        } else {
            model.addAttribute("title", "Hello, " + name + "! I hope you interested to hire me");
        }

        StringBuilder cvInString = new StringBuilder();

        cvInString.append("Name: Muhammad Fakhruddin Hafizh\n");
        cvInString.append("Birthplace: Jakarta\n");
        cvInString.append("Birthdate: August, 7, 1998\n");
        cvInString.append("Education History\n");
        cvInString.append("   -MI Al-Ihsan-   \n");
        cvInString.append("   -SMP 203 Jakarta-  \n");
        cvInString.append("   -SMA 14 Jakarta-   \n");
        cvInString.append("   -Fakultas Ilmu Komputer, Universitas Indonesia-   \n");
        model.addAttribute("cv", cvInString.toString());

        String description = "A second year computer science student who have great interest in software engineering, game development, and web development. " +
                "Currently, I looking for some intern program.\n";
        model.addAttribute("description", description);

        return "greeting";

    }

}
